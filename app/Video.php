<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = ['title', 'imageUrl', 'videoId'];

    public function video_category(){
        return $this->belongsTo(VideoCategory::class);
    }

    public function sub_videos(){
        return $this->hasMany(SubVideo::class);
    }
}
