<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubVideo extends Model
{
    protected $fillable = ['title', 'videoId'];

    public function video(){
        return $this->belongsTo(Video::class);
    }
}
