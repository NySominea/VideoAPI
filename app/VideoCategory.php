<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoCategory extends Model
{
    protected $fillable = ['name', 'imageUrl'];

    public function vidoes(){
        return $this->hasMany(Video::class);
    }
}
