<?php

use Illuminate\Database\Seeder;
use App\VideoCategory;
class VideoCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        VideoCategory::create([
            'name' => "Drama",
            'image' => 'https://www.brandcrowd.com/gallery/brands/pictures/picture13863063112963.jpg'
        ]);
        VideoCategory::create([
            'name' => "Action",
            'image' => 'https://comps.canstockphoto.com/clap-film-of-cinema-adventure-genre-clip-art_csp11392222.jpg'
        ]);
        VideoCategory::create([
            'name' => "Advanture",
            'image' => 'http://stuffpoint.com/adventure-movies/image/323259-adventure-movies-land-of-the-lost-movie-stills-wallpaper.jpg'
        ]);
        VideoCategory::create([
            'name' => "Romance",
            'image' => 'https://res.cloudinary.com/uktv/image/upload/h_450,q_80,w_800/v1474640968/uttlencimysvuz2gs41p.jpg'
        ]);
        VideoCategory::create([
            'name' => "Comedy",
            'image' => 'https://thumb7.shutterstock.com/display_pic_with_logo/882263/116548459/stock-photo-clap-film-of-cinema-fantasy-genre-clapperboard-text-illustration-116548459.jpg'
        ]);
        VideoCategory::create([
            'name' => "Crime",
            'image' => 'https://www.apf.com.au/Images/UserUploadedImages/501/DefaultVideoThumbnail.jpg'
        ]);
    }
}
