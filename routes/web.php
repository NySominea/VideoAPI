<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/videos',function(Illuminate\Http\Request $request){
    $data = $request;

    // return a video for search in a category
    if($request->input('query') && $request->input('category_name')){
        $category = App\VideoCategory::where("name",$data->input('category_name'))->get()->first();
        $video = App\Video::where('video_category_id',$category->id)
                        ->where('title',$request->input('query')) 
                        ->get()->first();
        $subVideos = App\SubVideo::where('video_id',$video->id)->get();
        $video->sub_videos = $subVideos?$subVideos:"";

        return [$video];
    }

    if($request->input('query')){
        $video = App\Video::where('title',$request->input('query')) 
                        ->get()->first();
        $subVideos = App\SubVideo::where('video_id',$video->id)->get();
        $video->sub_videos = $subVideos?$subVideos:"";

        return [$video];
    }

    // return all vidoes for a specific category
    if($request->input('category_name')){
        $category = App\VideoCategory::where("name",$data->input('category_name'))->get()->first();
        $videos = App\Video::where('video_category_id',$category->id)->get();

        foreach($videos as $video){
            $subVideos = App\SubVideo::where('video_id',$video->id)->get();
            $video->sub_videos = $subVideos?$subVideos:"";
        }

        return $videos;
        
    }


    // Return all videos for home page
    $allVideos = App\Video::all();
    foreach($allVideos as $video){
        $subVideos = App\SubVideo::where('video_id',$video->id)->get();
        $video["sub_videos"] = $subVideos?$subVideos:"";
    }
    return $allVideos;

});


Route::get('getRecents',function(Illuminate\Http\Request $request){

    // ****Order By lastest Recents*******//

    if($request->input('videos_id')){
        $array = $request->input('videos_id');  
        $placeholders = implode(',',array_fill(0, count($array), '?'));
        $recents = App\Video::whereIn('id',$array)->orderByRaw("field(id,{$placeholders})", $array)->get();
        foreach($recents as $recent){
            $subVideos = App\SubVideo::where('video_id',$recent->id)->get();
            $recent["sub_videos"] = $subVideos?$subVideos:"";
        }
        return $recents;
    }
    return [];
});


Route::get('/search',function(Illuminate\Http\Request $request){
    $data = $request;

    // return array of video title for suggestion search in a category
    if($data->input('category_name')){
        if(!$data->input('query')){
            return [];
        }
        $category = App\VideoCategory::where("name",$data->input('category_name'))->get()->first();
        $resultTitle = App\Video::where('video_category_id',$category->id)
                                ->where('title','like','%'.$data->input('query').'%')->pluck('title');
        return $resultTitle;
    }

    // return array of video title for suggestion search for all videos
    if(!$data->input('query')){
        return [];
    }
    $resultTitle = App\Video::where('title','like','%'.$data->input('query').'%')->pluck('title');
    return $resultTitle;
});



Route::get('/categories',function(){
    $categories = App\VideoCategory::all();
    return $categories;
});

Route::get('/getNewVideos',function(){
    $videos = App\Video::all();
    foreach($videos as $video){
        $subVideos = App\SubVideo::where('video_id',$video->id)->get();
        $video->sub_videos = $subVideos?$subVideos:"";
    }
    return response()->json($videos);
});

Route::get('/getHit',function(){
    $videos = App\Video::all();
    foreach($videos as $video){
        $subVideos = App\SubVideo::where('video_id',$video->id)->get();
        $video->sub_videos = $subVideos?$subVideos:"";
    }
    return response()->json($videos);
});
