<?php

use Illuminate\Database\Seeder;
use App\SubVideo;
class SubVideosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SubVideo::create([
            "title" => "The Flash 2",
            "videoId" => "1aGkEpUi5No",
            "video_id" => 5
        ]);

        SubVideo::create([
            "title" => "The Flash 3",
            "videoId" => "6IpZr09PNkE",
            "video_id" => 5
        ]);
    }
}
