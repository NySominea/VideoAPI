<?php

use Illuminate\Database\Seeder;
use App\Video;
class VideosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Video::create([
            'title' => "Knightfall",
            'imageUrl' => "http://cdn.osxdaily.com/wp-content/uploads/2016/10/YouTube-icon-full_color-610x430.png",
            'videoId' => "Mo3qQ5m8YfM",
            "video_category_id" => "2",
            "sub_videos" => ""
        ]);
        Video::create([
            'title' => "Justice League",
            'imageUrl' => "http://cdn.osxdaily.com/wp-content/uploads/2016/10/YouTube-icon-full_color-610x430.png",
            'videoId' => "Mo3qQ5m8YfM",
            "video_category_id" => "2",
            "sub_videos" => ""
        ]);
        Video::create([
            'title' => "The Killer",
            'imageUrl' => "http://cdn.osxdaily.com/wp-content/uploads/2016/10/YouTube-icon-full_color-610x430.png",
            'videoId' => "Mo3qQ5m8YfM",
            "video_category_id" => "2",
            "sub_videos" => ""
        ]);
        Video::create([
            'title' => "I Love You, Daddy",
            'imageUrl' => "http://cdn.osxdaily.com/wp-content/uploads/2016/10/YouTube-icon-full_color-610x430.png",
            'videoId' => "Mo3qQ5m8YfM",
            "video_category_id" => "5",
            "sub_videos" => ""
        ]);
        Video::create([
            'title' => "The Flash",
            'imageUrl' => "http://cdn.osxdaily.com/wp-content/uploads/2016/10/YouTube-icon-full_color-610x430.png",
            'videoId' => "_LY7XFHM5Tk",
            "video_category_id" => "1",
            "sub_videos" => ""
        ]);
        Video::create([
            'title' => "Shattered",
            'imageUrl' => "http://cdn.osxdaily.com/wp-content/uploads/2016/10/YouTube-icon-full_color-610x430.png",
            'videoId' => "Mo3qQ5m8YfM",
            "video_category_id" => "1",
            "sub_videos" => ""
        ]);
        Video::create([
            'title' => "Dream House Nightmare",
            'imageUrl' => "http://cdn.osxdaily.com/wp-content/uploads/2016/10/YouTube-icon-full_color-610x430.png",
            'videoId' => "Mo3qQ5m8YfM",
            "video_category_id" => "1",
            "sub_videos" => ""
        ]);
        Video::create([
            'title' => "Absolute Power",
            'imageUrl' => "http://cdn.osxdaily.com/wp-content/uploads/2016/10/YouTube-icon-full_color-610x430.png",
            'videoId' => "Mo3qQ5m8YfM",
            "video_category_id" => "6",
            "sub_videos" => ""
        ]);
        Video::create([
            'title' => "The Snowman",
            'imageUrl' => "http://cdn.osxdaily.com/wp-content/uploads/2016/10/YouTube-icon-full_color-610x430.png",
            'videoId' => "Mo3qQ5m8YfM",
            "video_category_id" => "6",
            "sub_videos" => ""
        ]);
    }
}
